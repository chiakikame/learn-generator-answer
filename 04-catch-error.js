function *upper(items) {
  for (let item of items) {
    let result = null;
    try {
      result = item.toUpperCase();
    } catch (err) {
      result = null;
    }
    
    yield result;
  }
}

var bad_items = ['a', 'B', 1, 'c'];

for (var item of upper(bad_items)) {
  console.log(item);
}
