function askFoo() {
  return new Promise(function (resolve, reject) {
    resolve('foo');
  });
}

function run(generator) {
  let iter = generator();
  
  function go(result) {
    if (result.done) return result.value;
    
    result.value.then(
      result => go(iter.next(result)),
      err => go(iter.throw(err))
    );
  }
  
  go(iter.next());
}

run(function* (){
  let foo = null;
  try {
    foo = yield askFoo();
  } catch (err) {
    foo = null;
  }
  console.log(foo);
})
