let fs = require('fs');

function run (generator) {
  let iterator = generator(go);
  
  function go(err, result) {
    if (err) {
      iterator.throw(err);
    } else {
      iterator.next(result);
    }
  }
  
  go();
}

run(function* (done) {
  // catch exception
  let firstFile = null;
  try {
    let dirFiles = yield fs.readdir('NoNoNoNo', done);
    firstFile = dirFiles[0];
  } catch (err) {
    firstFile = null;
  }
  
  console.log(firstFile);
});
