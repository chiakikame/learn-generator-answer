This is a personal answer repo of `learn-generator` workshop.

## Important aspects

* Generator syntax
* Generator are iterators
* Recursive generators are possible
* `try / catch` is available in generators
* Hide asynchronicity

The concept of exercise 05 and 06 are hard ...
