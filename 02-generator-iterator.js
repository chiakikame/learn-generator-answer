function *factorial(n) {
  for(let i = 1; i <= n; i++) {
    let acc = 1;
    for (let j = 1; j <= i; j++) {
      acc *= j;
    }
    yield acc;
  }
}

for (var n of factorial(5)) {
  console.log(n);
}
